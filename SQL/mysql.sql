CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;
use mywebsite;

CREATE TABLE user(
user_id int PRIMARY KEY AUTO_INCREMENT,
login_id int NOT NULL UNIQUE,
password varchar(256),
name varchar(256) NOT NULL,
birthday date NOT NULL,
address varchar(256) NOT NULL,
email varchar(256) NOT NULL,
create_date datetime NOT NULL,
up_date datetime NOT NULL);

CREATE TABLE orders(
order_id int PRIMARY KEY AUTO_INCREMENT,
total_price int NOT NULL,
orderday datetime NOT NULL,
user_id int NOT NULL,
delivery_id int NOT NULL);

CREATE TABLE delivery_method(
delivery_id int PRIMARY KEY AUTO_INCREMENT,
delivery_name varchar(256) NOT NULL,
delivery_price int NOT NULL);

CREATE TABLE order_detail(
oder_detail_id int PRIMARY KEY AUTO_INCREMENT,
order_id int NOT NULL,
product_id int NOT NULL);


CREATE TABLE product(
product_id int PRIMARY KEY AUTO_INCREMENT,
product_name  varchar(256) NOT NULL,
product_detail varchar(256) NOT NULL, 
product_price int NOT NULL,
file_name varchar(256) NOT NULL);

CREATE TABLE favorite(
favorite_id int PRIMARY KEY AUTO_INCREMENT,
product_id int NOT NULL,
user_id int NOT NULL);

INSERT INTO delivery_method(
delivery_name,
delivery_price )VALUES('特急配送',500);

INSERT INTO delivery_method(
delivery_name,
delivery_price )VALUES('日時指定',300);

INSERT INTO delivery_method(
delivery_name,
delivery_price )VALUES('普通配送',0);

INSERT INTO product(
product_name,
product_detail, 
product_price,
file_name)
VALUES ('hair oil','髪になじませた瞬間に浸透し、ダメージを受けた髪を補修する美容液のようなヘアオイル。毛先までうるおいに満ちた髪は、スタイリングしやすくまとまりやすくなります。',2000,'hairoil.png');

INSERT INTO product(
product_name,
product_detail, 
product_price,
file_name)
VALUES ('hair oil','髪になじませた瞬間に浸透し、ダメージを受けた髪を補修する美容液のようなヘアオイル。毛先までうるおいに満ちた髪は、スタイリングしやすくまとまりやすくなります。',2000,'hairoil.png');

INSERT INTO product(
product_name,
product_detail, 
product_price,
file_name)
VALUES ('hair oil','髪になじませた瞬間に浸透し、ダメージを受けた髪を補修する美容液のようなヘアオイル。毛先までうるおいに満ちた髪は、スタイリングしやすくまとまりやすくなります。',2000,'hairoil.png');

INSERT INTO product(
product_name,
product_detail, 
product_price,
file_name)
VALUES ('hair oil','髪になじませた瞬間に浸透し、ダメージを受けた髪を補修する美容液のようなヘアオイル。毛先までうるおいに満ちた髪は、スタイリングしやすくまとまりやすくなります。',2000,'hairoil.png');

INSERT INTO product(
product_name,
product_detail, 
product_price,
file_name)
VALUES ('hair oil','髪になじませた瞬間に浸透し、ダメージを受けた髪を補修する美容液のようなヘアオイル。毛先までうるおいに満ちた髪は、スタイリングしやすくまとまりやすくなります。',2000,'hairoil.png');

INSERT INTO product(
product_name,
product_detail, 
product_price,
file_name)
VALUES ('hair oil','髪になじませた瞬間に浸透し、ダメージを受けた髪を補修する美容液のようなヘアオイル。毛先までうるおいに満ちた髪は、スタイリングしやすくまとまりやすくなります。',2000,'hairoil.png');
